#!/usr/bin/env python
import rospy
import tf
from math import hypot, atan2, pi
from nav_msgs.msg import Path, Odometry
from geometry_msgs.msg import Pose2D, Twist, PoseStamped, Quaternion
from std_msgs.msg import String

class PathFollower(object):
    def __init__(self):
        rospy.init_node("path_follower")
        self.base_v = rospy.get_param("/path_follower/base_v", 0.3)
        self.min_v = rospy.get_param("/path_follower/min_v", 0.05)
        self.rate = rospy.get_param("/path_follower/rate", 10)
        self.accuracy_pose = rospy.get_param("/path_follower/accuracy_pose", 0.01)
        self.accuracy_angle = rospy.get_param("/path_follower/accuracy_angle", 0.01)
        self.min_dist_to_goal = rospy.get_param("/path_follower/min_dist_to_goal", 0.2)
        self.look_ahead_index = rospy.get_param("/path_follower/look_ahead_index", 40)
        self.dist_to_start_pid = rospy.get_param("/path_follower/dist_to_start_pid", 0.4)
        self.max_rotation_speed = rospy.get_param("/path_follower/max_rotation_speed", 1.3)
        self.robot_coordinates = None
        self.actual_path = None
        self.path_follower_timer = None
        self.goal_angle = None

        self.pid_path = PidRegulator(1.5, 0, 3)
        self.pid_stop = PidRegulator(1.5, 0, 3)

        # self.robot_coordinates_sub = rospy.Subscriber("/robot_coords", Pose2D, self.robot_coordinates_callback)
        self.robot_coordinates_sub = rospy.Subscriber("/odom", Odometry, self.robot_coordinates_callback)
        self.input_path = rospy.Subscriber("/actual_path", Path, self.input_path_callback)

        self.velocity_publisher = rospy.Publisher("/cmd_vel", Twist, queue_size=1)
        self.status_publisher = rospy.Publisher("/path_follower/status", String, queue_size=1)
        self.path_publisher = rospy.Publisher("/path_follower/path", Path, queue_size=1)
        self.pose_pub = rospy.Publisher("/path_follower/target", PoseStamped, queue_size=1)

    # def robot_coordinates_callback(self, msg):
    #     self.robot_coordinates = msg

    def robot_coordinates_callback(self, msg):
        self.robot_coordinates = Pose2D()
        self.robot_coordinates.x = msg.pose.pose.position.x
        self.robot_coordinates.y = msg.pose.pose.position.y
        quat = msg.pose.pose.orientation.x , msg.pose.pose.orientation.y, msg.pose.pose.orientation.z, msg.pose.pose.orientation.w
        self.robot_coordinates.theta = self.wrap_angle((tf.transformations.euler_from_quaternion(quat))[2])

    def input_path_callback(self, msg):
        self.actual_path = msg
        quat = self.actual_path.poses[-1].pose.orientation.x, self.actual_path.poses[-1].pose.orientation.y, \
               self.actual_path.poses[-1].pose.orientation.z, self.actual_path.poses[-1].pose.orientation.w 
        self.goal_angle = (tf.transformations.euler_from_quaternion(quat))[2]
        self.path_publisher.publish(self.actual_path)
        self.path_follower_main_function()
    
    def path_follower_main_function(self):
        current_index = self.get_nearest_point_index()
        alpha = atan2(self.actual_path.poses[current_index].pose.position.y - self.robot_coordinates.y, \
                      self.actual_path.poses[current_index].pose.position.x - self.robot_coordinates.x)
        
        while abs(self.robot_coordinates.theta - alpha) > self.accuracy_angle:
            rospy.loginfo("target_angle %s robot_angle %s", alpha, self.robot_coordinates.theta)
            self.publish_pose(self.robot_coordinates, alpha)
            vel = Twist()
            vel.angular.z = self.pid_path.get_value(alpha - self.robot_coordinates.theta)
            self.velocity_publisher.publish(vel)
            rospy.sleep(0.01)

        vel = Twist()
        self.velocity_publisher.publish(vel)
        rospy.loginfo("Finish rotation %s", self.robot_coordinates.theta)

        while abs(self.robot_coordinates.x - self.actual_path.poses[-1].pose.position.x) > self.accuracy_pose or \
              abs(self.robot_coordinates.y - self.actual_path.poses[-1].pose.position.y) > self.accuracy_pose:
            current_index = self.get_nearest_point_index()
            alpha = atan2(self.actual_path.poses[current_index].pose.position.y - self.robot_coordinates.y, \
                          self.actual_path.poses[current_index].pose.position.x - self.robot_coordinates.x)
            vel = Twist()
            if self.get_dist_to_goal() > self.dist_to_start_pid:
                vel.linear.x = self.base_v
            else:
                vel.linear.x = max(self.min_v, self.pid_stop.get_value(self.get_dist_to_goal()))

            vel.angular.z = min(self.max_rotation_speed, self.pid_path.get_value(alpha - self.robot_coordinates.theta))
            self.publish_pose(self.robot_coordinates, alpha)
            self.velocity_publisher.publish(vel)
            rospy.sleep(0.1)
            rospy.loginfo("Moving %s %s %s", self.robot_coordinates, alpha, current_index)
        vel = Twist()
        self.velocity_publisher.publish(vel)

        while abs(self.robot_coordinates.theta - self.goal_angle) > self.accuracy_angle:
            rospy.loginfo("target_angle %s robot_angle %s", self.goal_angle, self.robot_coordinates.theta)
            self.publish_pose(self.robot_coordinates, self.goal_angle)
            vel = Twist()
            vel.angular.z = min(self.max_rotation_speed, self.pid_path.get_value(self.goal_angle - self.robot_coordinates.theta))
            self.velocity_publisher.publish(vel)
            rospy.sleep(0.01)

        vel = Twist()
        self.velocity_publisher.publish(vel)
        rospy.loginfo("FInish")


    def publish_pose(self, coords, angle):
        pos = PoseStamped()
        pos.header.frame_id = "odom"
        pos.header.stamp = rospy.Time.now()
        pos.pose.position.x = coords.x
        pos.pose.position.y = coords.y
        quat = tf.transformations.quaternion_from_euler(0, 0, angle)
        pos.pose.orientation = Quaternion(quat[0], quat[1], quat[2], quat[3])
        self.pose_pub.publish(pos)

    def get_nearest_point_index(self):
        # find nearest index in path
        if abs(self.robot_coordinates.x - self.actual_path.poses[-1].pose.position.x) < self.min_dist_to_goal or \
           abs(self.robot_coordinates.y - self.actual_path.poses[-1].pose.position.y) < self.min_dist_to_goal:
           return -1
        
        min_index = 0
        min_dist = 10000
        for i in range(len(self.actual_path.poses)):
            if hypot(self.actual_path.poses[i].pose.position.x - self.robot_coordinates.x, \
                     self.actual_path.poses[i].pose.position.y - self.robot_coordinates.y) < min_dist:
                min_dist = hypot(self.actual_path.poses[i].pose.position.x - self.robot_coordinates.x, \
                                 self.actual_path.poses[i].pose.position.y - self.robot_coordinates.y)
                min_index = i
        min_index = min([min_index + self.look_ahead_index, len(self.actual_path.poses)])
        return min_index
    
    def get_dist_to_goal(self):
        return hypot(self.actual_path.poses[-1].pose.position.x - self.robot_coordinates.x, \
                     self.actual_path.poses[-1].pose.position.y - self.robot_coordinates.y)

    def wrap_angle(self, angle):
        pi2 = 2 * pi

        while angle < -pi:
            angle += pi2

        while angle >= pi:
            angle -= pi2

        return angle

class PidRegulator(object):
    def __init__(self, Kp, Ki, Kd, Integrator_max=2):
        self.Integrator = 0.0
        self.Derivator = 0.0
        self.Integrator_max = Integrator_max
        self.Kp = Kp
        self.Ki = Ki
        self.Kd = Kd

    def get_value(self, error):
        if self.Integrator >= self.Integrator_max:
            self.Integrator = self.Integrator_max
        else:
            self.Integrator += error
        self.I_val = self.Ki * self.Integrator
        self.D_val = self.Kd * (error - self.Derivator)
        self.Derivator = error

        res = self.Kp * error + self.I_val + self.D_val
        return res


def main():
    path_follower = PathFollower()
    rospy.spin()

if __name__ == "__main__":
    main()
    